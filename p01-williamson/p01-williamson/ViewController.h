//
//  ViewController.h
//  p01-williamson
//
//  File edited from default to add action on label upon click
//
//  Created by Matthew Williamson on 1/27/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *helloWorldLabel;

@property (nonatomic, strong) IBOutlet UIButton *eventButton;

- (IBAction)clicked:(id)sender;

@end

